require 'rails_helper'

RSpec.describe Administrator do
  describe "#password=" do
    example "given string should be hashed_password is string and length is 60" do
      admin = Administrator.new
      admin.password = 'baukis'
      expect(admin.hashed_password).to be_kind_of(String)
      expect(admin.hashed_password.size).to eq(60)
    end

    example "given nil should be hashed_password is nil" do
      admin = Administrator.new(hashed_password: 'x')
      admin.password = nil
      expect(admin.hashed_password).to be_nil
    end
  end
end
