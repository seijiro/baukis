require 'rails_helper'

RSpec.describe StaffMember do
  describe "#password=" do
    example "given string should be hashed_password is string and length is 60" do
      member = StaffMember.new
      member.password = 'baukis'
      expect(member.hashed_password).to be_kind_of(String)
      expect(member.hashed_password.size).to eq(60)
    end

    example "given nil should be hashed_password is nil" do
      member = StaffMember.new(hashed_password: 'x')
      member.password = nil
      expect(member.hashed_password).to be_nil
    end
  end
end
