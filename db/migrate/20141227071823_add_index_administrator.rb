class AddIndexAdministrator < ActiveRecord::Migration
  def change
    add_index :administrators, :email_for_index, unique: true
  end
end
